api = 2
core = 7.x

projects[views] = 3.3
projects[wysiwyg] = 2.1
projects[wysiwyg_template] = 2.6

; Defining a theme is no different.
projects[rubik] = 4.0-beta8
projects[tao] = 3.0-beta4
projects[cube] = 1.1
projects[context] = 3.0-beta2

; You can even specify patches to be applied to external library code,
; so long as the patches themselves live on Drupal.org.
libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.tar.gz"
libraries[ckeditor][destination] = libraries
libraries[ckeditor][directory_name] = ckeditor